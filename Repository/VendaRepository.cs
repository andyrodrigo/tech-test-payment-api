using tech_test_payment_api.Models;
using tech_test_payment_api.DataBase;

namespace tech_test_payment_api.Repository
{
    public class VendaRepository : IVendaRepository
    {
        private Context_DB _db;

        public VendaRepository()
        {
            _db = new Context_DB();
        }

        public bool CreateVenda( Venda venda )
        {
            return _db.Add( venda );
        }

        public Venda GetVenda( int id )
        {
            return _db.Find( id );
        }

        public bool UpdateVenda( Venda venda )
        {
            return _db.UpdateVenda( venda );
        }
    }
}