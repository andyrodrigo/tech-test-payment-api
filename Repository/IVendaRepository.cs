using tech_test_payment_api.Models;

namespace tech_test_payment_api.Repository
{
    public interface IVendaRepository
    {
        bool CreateVenda( Venda venda );
        Venda GetVenda( int id );
        bool UpdateVenda( Venda venda );
    }
}