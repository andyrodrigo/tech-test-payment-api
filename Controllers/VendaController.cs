using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Models.Dtos;
using tech_test_payment_api.Repository;
using tech_test_payment_api.Services;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route( "[controller]" )]
    public class VendaController : ControllerBase
    {
        private readonly IVendaRepository _vendaRepository;
        private readonly VendaService _vendaService;

        public VendaController( IVendaRepository vendaRepository )
        {
            _vendaRepository = vendaRepository;
            _vendaService = new VendaService();
        }

        [HttpPost ( "RegistrarVenda" )]
        public IActionResult RegistrarVenda( VendaDto dto )
        {
            if( dto.Itens.Length == 0 )
                return BadRequest( ModelState );

            Venda venda = new Venda( dto );
            if( !_vendaRepository.CreateVenda( venda ) ){
                ModelState.AddModelError( "" , $"Algo deu errado durante a gravação da venda: { venda.Itens }" );
                return StatusCode( 500 , ModelState );
            }
            return CreatedAtAction( nameof( BuscarVenda ) , new { id = venda.Id } , venda );
        }

        [HttpGet ( "BuscarVenda/{id}" )]
        public IActionResult BuscarVenda( int id )
        {
            var venda = _vendaRepository.GetVenda( id );

            if( venda != null )
                return Ok( venda );

            return NotFound();
        }

        [HttpPut ( "AtualizarVenda" )]
        public IActionResult AtualizarVenda( Venda venda, bool progressoDaVenda )
        {
            VendaResponse resposta = _vendaService.mudarStatus( venda , progressoDaVenda );
            if( resposta.Validator ){
                if( !_vendaRepository.UpdateVenda( resposta.Venda ) ){
                    ModelState.AddModelError( "" , $"Algo deu errado durante a mudança de status da venda: { venda.Itens }" );
                    return StatusCode( 500, ModelState );
                }
            }else{
                ModelState.AddModelError( "" , $"Mudança de status não permitida!" );
                return StatusCode( 500 , ModelState );
            }
            return NoContent();
        }
        
    }
}