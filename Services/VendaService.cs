using tech_test_payment_api.Models;
using tech_test_payment_api.Models.Dtos;

namespace tech_test_payment_api.Services
{
    public class VendaService
    {
        const string cancelMsg = "Cancelada";
        const string waitMsg = "Aguardando Pgto";
        const string aprovalMsg = "Pagamento Aprovado";
        const string sendMsg = "Enviado para a Transportadora";
        const string deliveredMsg = "Entregue";

        public VendaResponse mudarStatus( Venda venda , bool progressoDaVenda )
        {
            VendaResponse resposta = new VendaResponse();
            resposta.Venda = venda;

            //Condições em que não se permite mais a mudança de status:
            if( venda.Status == cancelMsg || venda.Status == deliveredMsg || ( venda.Status == sendMsg && progressoDaVenda == false ) ){
                resposta.Validator = false;

            //Condições de progresso de venda:
            }else if( venda.Status == waitMsg || venda.Status == aprovalMsg || venda.Status == sendMsg ){
                resposta.Validator = true;
                if( progressoDaVenda ){
                    if( venda.Status == waitMsg ){
                        resposta.Venda.Status = aprovalMsg;
                    }else if( venda.Status == aprovalMsg ){
                        resposta.Venda.Status = sendMsg;
                    }else{ //( venda.Status == sendMsg )
                        resposta.Venda.Status = deliveredMsg;
                    }
                }else{ //pedido de cancelamento da venda aceito
                    resposta.Venda.Status = cancelMsg;
                }
            }else{ //Venda.Status fora de padrão
                resposta.Validator = false;
            }
            return resposta;
        }     
    }
}