namespace tech_test_payment_api.Models
{
    public class Vendedor
    {
        public int Id { get; set; }
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }

        public Vendedor( int id , string cpf , string nome , string email , string telefone )
        {
            this.Id = id;
            this.Cpf = cpf;
            this.Nome = nome;
            this.Email = email;
            this.Telefone = telefone;
        } 
    }
}