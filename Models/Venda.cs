using tech_test_payment_api.Models.Dtos;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public string[] Itens { get; set; }
        public Vendedor Vendedor { get; set; }
        public DateTime Data { get; set; }
        public string Status { get; set; }

        public Venda(){}

        public Venda( int id , string[] itens , Vendedor vendedor , DateTime data , string status )
        {
            this.Id = id;
            this.Itens = itens;
            this.Vendedor = vendedor;
            this.Status = status;
            this.Data = data;
        }

        public Venda( VendaDto dto ){
            this.Itens = dto.Itens;
            this.Vendedor = dto.Vendedor;
            this.Status = "Aguardando Pgto";
            this.Data = DateTime.Today;
        }
    }
}