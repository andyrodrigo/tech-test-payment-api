using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models.Dtos
{
    public class VendaDto
    {
        public string[] Itens { get; set; }
        public Vendedor Vendedor { get; set; }
    }
}