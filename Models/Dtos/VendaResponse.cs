using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models.Dtos
{
    public class VendaResponse
    {
        public bool Validator { get; set; }
        public Venda Venda { get ; set; }       
    }
}