using tech_test_payment_api.Models;
using Newtonsoft.Json;

namespace tech_test_payment_api.DataBase
{
    public class Context_DB
    {
        private string DataBaseFile = "Dados.txt";
        private List<Venda> Vendas = new List<Venda>();

        private void GetDataBase()
        {
            string leitura = "";
            try{
                StreamReader arquivo;
                arquivo = new StreamReader( DataBaseFile );
                string linha;
                while( ( linha = arquivo.ReadLine() ) != null ){
                    leitura = linha;
                    Venda venda = JsonConvert.DeserializeObject<Venda>(linha);
                    Vendas.Add( venda );
                }
                arquivo.Close();
            }catch( Exception exception ){
                leitura = exception.Message;
                Console.WriteLine( leitura );
            }
        }

        private int MakeId()
        {
            int identificador = 1;
            try{
                StreamReader arquivo;
                arquivo = new StreamReader( DataBaseFile );
                string linha;
                while( ( linha = arquivo.ReadLine() ) != null ){
                    identificador++;
                }
                arquivo.Close();
            }catch( Exception exception ){
                Console.WriteLine( exception.Message );
            }
            return identificador;
        }

        private void lineChanger(string strVenda, string fileName, int line_to_edit)
        {
            string[] arrLine = File.ReadAllLines( fileName );
            arrLine[line_to_edit - 1] = strVenda;
            File.WriteAllLines(fileName, arrLine);
        }

        public bool Add( Venda venda )
        {
            try{
                venda.Id = MakeId();
                Vendas.Add( venda );
                string strVenda = JsonConvert.SerializeObject( venda );
                using( StreamWriter arquivo = File.AppendText( DataBaseFile ) )
                {
                    arquivo.WriteLine( strVenda ); 
                    arquivo.Close();
                }
                return true;

            } catch ( Exception exception  ){
                Console.WriteLine( exception.Message );
                return false;
            }
        }

        public Venda Find( int id )
        {
            GetDataBase();
            return this.Vendas.Find( x => x.Id == id );
        }

        public bool UpdateVenda( Venda venda )
        {
            try{
                Venda vendaDb = Find( venda.Id );
                string strVenda = JsonConvert.SerializeObject( venda );
                lineChanger( strVenda, DataBaseFile , vendaDb.Id );
                return true;
            } catch ( Exception exception){
                Console.WriteLine( exception.Message );
                return false;
            }
        }
    }
}